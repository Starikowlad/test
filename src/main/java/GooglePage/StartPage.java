package GooglePage;

import org.openqa.selenium.Keys;

import java.time.Duration;

public class StartPage extends BasePage {
    public SearchResultPage inputTextInSearchField(String text) {
        findByCSS(".devsite-searchbox>input", Duration.ofSeconds(10)).sendKeys(text, Keys.ENTER);
        return new SearchResultPage();
    }
}
