package GooglePage;

import driverManager.DriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import services.TestDataReader;

import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class BasePage {
    private static final String  HOMEPAGE_URL = "https://cloud.google.com/";
    WebDriver driver;

    public BasePage() {
        this.driver = DriverManager.getDriver();
    }

    public WebElement findByCSS(String css, Duration duration) {
        return driver.findElement(waitForElementVisible(By.cssSelector(css), duration.getSeconds()));
    }

    public WebElement findByText(String text, Duration duration) {
        return driver.findElement(waitForElementVisible(By.xpath("//*[text()=\"" + text + "\"]"), duration.getSeconds()));
    }
    public StartPage goToStartPage() {
        DriverManager.getDriver().get(HOMEPAGE_URL);
        return new StartPage();
    }

    private WebDriverWait getWaiter(long timeOutInSeconds) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeOutInSeconds);
        webDriverWait.ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(StaleElementReferenceException.class);
        return webDriverWait;
    }
    public By waitForElementVisible(By findStrategy, long timeOutInSeconds) {
        getWaiter(timeOutInSeconds).until(visibilityOfElementLocated(findStrategy));
        return findStrategy;
    }
}
