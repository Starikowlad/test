package GooglePage;

import java.time.Duration;

public class SearchResultPage extends BasePage {
    public CalculatorPage clickByText(String text) {
        findByText(text, Duration.ofSeconds(10)).click();
        return new CalculatorPage();
    }
}
