package GooglePage;

import java.time.Duration;

public class CalculatorPage extends BasePage {
    public CalculatorPage inputNumberOfInstances(String number) {
//        try {
//            Thread.sleep(500000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        findByCSS("input[ng-model='listingCtrl.computeServer.quantity']", Duration.ofSeconds(10)).click();
        findByCSS("input[ng-model='listingCtrl.computeServer.quantity']", Duration.ofSeconds(10)).sendKeys(number);
        return this;
    }
    public CalculatorPage inputNumberOfNodes(String number) {
        findByCSS("[ng-model='listingCtrl.soleTenant.nodesCount'", Duration.ofSeconds(10)).sendKeys(number);
        return this;
    }
    public CalculatorPage clickEmailEstimate() {
        findByCSS("[aria-label='Email Estimate']", Duration.ofSeconds(10)).click();
        return this;
    }
    public CalculatorPage inputEmail(String email) {
        findByCSS("[ng-model='emailQuote.user.email']", Duration.ofSeconds(10)).sendKeys(email);
        return this;
    }
    public CalculatorPage clickOnAddToEstimate() {
        findByCSS("button[ng-disabled=\"ComputeEngineForm.$invalid || !listingCtrl.machineType['computeServer']\"]", Duration.ofSeconds(10)).click();
        return this;
    }
    public CalculatorPage clickOnSendEmail() {
        findByCSS("button[ng-click='emailQuote.emailQuote(true); emailQuote.$mdDialog.hide()']", Duration.ofSeconds(10)).click();
        return this;
    }

}
