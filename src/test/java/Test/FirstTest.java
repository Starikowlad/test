package Test;//package Test;
//
//import org.openqa.selenium.By;
//import org.testng.Assert;
//import org.testng.annotations.Test;
//import ru.yandex.qatools.ashot.AShot;
//import ru.yandex.qatools.ashot.Screenshot;
//import ru.yandex.qatools.ashot.comparison.ImageDiff;
//import ru.yandex.qatools.ashot.comparison.ImageDiffer;
//import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
//import ru.yandex.qatools.ashot.shooting.*;
//
//import javax.imageio.ImageIO;
//import java.awt.image.BufferedImage;
//import java.io.File;
//import java.io.IOException;
//
//public class FirstTest extends BaseTest {
//    @Test
//    public void firstTest() {
//        driver.get("https://www.google.com/");
//        Screenshot expectedScreenshot = new AShot()
//                .coordsProvider(new WebDriverCoordsProvider())
//                .shootingStrategy(ShootingStrategies.viewportPasting(ShootingStrategies.scaling(2f), 1000))
//                .addIgnoredElement(By.cssSelector("form[action]"))
//                .takeScreenshot(driver);
//        driver.findElement(By.cssSelector("input[title]")).click();
//        driver.findElement(By.cssSelector("input[title]")).sendKeys("qqqqq");
//        Screenshot actualScreenshot = new AShot()
//                .coordsProvider(new WebDriverCoordsProvider())
//                .shootingStrategy(ShootingStrategies.viewportPasting(ShootingStrategies.scaling(2f), 1000))
//                .addIgnoredElement(By.cssSelector("form[action]"))
//                .takeScreenshot(driver);
//        ImageDiff imageDiff = new ImageDiffer().makeDiff(expectedScreenshot,actualScreenshot);
//        BufferedImage bufferedImage = imageDiff.getMarkedImage();
//        System.out.println(imageDiff.getDiffSize());
//        try {
//            ImageIO.write(bufferedImage, "png", new File("/Users/Vladislav_Starikov/ideaProject/Test/actual.png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        Assert.assertFalse(imageDiff.hasDiff());
//    }
//}