package Test;


import GooglePage.StartPage;
import org.testng.annotations.Test;

public class HardCoreTest extends BaseTest {
    @Test(description = "Google Cloud Platform Pricing Calculator")
    public void hardCoreTest() {
        new StartPage().goToStartPage()
                .inputTextInSearchField("Google Cloud Platform Pricing Calculator")
                .clickByText("Google Cloud Pricing Calculator")
                .inputNumberOfInstances("4")
                .clickOnAddToEstimate()
                .clickEmailEstimate()
                .inputEmail("vst4riko@yopmail.com")
                .clickOnSendEmail();
    }
}
